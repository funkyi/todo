export class TodoModel {
  id: string;
  subject: string;
  note: string;
  remark: string;
  starred: boolean = false;
  dateAdded: number;
  timeAdded: string;
  
  constructor () { }
}