export class UserModel {
  uId: string;
  fbId: string;
  displayName: string;
  photoURL: string;
  email: string;  

  constructor () { }
}