import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';

@Injectable()
export class FirebaseService {
    user: firebase.User;
    todolist: FirebaseListObservable<any[]>;
    listOfTodo: any = [];

    constructor(private afAuth: AngularFireAuth, private afDB: AngularFireDatabase, private toastCtrl: ToastController) {
        afAuth.authState.subscribe(user => {
            if (!user)
                return;
            
            this.userInfo = user;
        });
    }

    set userInfo (u: firebase.User) {
        this.user = u;
    }

    get userInfo(): firebase.User {
        return this.user;
    }

    showToast(message: string, position: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: position
        });

        toast.present(toast);
    }

    getTodoDetail(values: any[], id: string) {
        if (typeof values.find(item => item.id === id) !== 'undefined') {
            // console.log(values.find(item => item.id === id) );
            return values.find(item => item.id === id);
        }

        return false;
    }

    loadTodo(db: string, queryString: any = {}) {
        const queryObservable = this.afDB.list('/' + db, queryString);

        return queryObservable;
    }

    OrderByArray(values: any[], orderType: any, sortOrder: string = "DESC") {
        // let vals: any = values.slice(0);
        return values.sort((a, b) => {
            if (a[orderType] < b[orderType]) {
                return (sortOrder == 'DESC') ? 1 : -1;
            }

            if (a[orderType] > b[orderType]) {
                return (sortOrder == 'DESC') ? -1 : 1;
            }

            return 0
        });

    }

    convertDt(dt: string) {
        let daydate = new Date(dt);        
        return daydate;
    }

    getTimestamp(dt: Date = new Date()) {
        let timestamp = Math.floor(dt.getTime() / 1000);
        return timestamp;
    }

    updateData(key: string, values: any, db: string = '', showToast: boolean = true) {
        if (db == '')
            db = this.userInfo.uid + '/todo';

        this.afDB.list('/' + db).update(key, values);

        if (showToast)
            this.showToast("TODO, update successful", "bottom");
    }

    writeData(params: any, db: string = '') {
        if (db == '')
            db = this.userInfo.uid + '/todo';

        this.afDB.list('/' + db).push(params);
        this.showToast("TODO, save successful", "bottom");
    }

    removeData(param: string, db: string = '') {
        if (db == '')
            db = this.userInfo.uid + '/todo';

        this.afDB.list('/' + db).remove(param);
    }

    login() {
        return this.afAuth.auth
            .signInWithPopup(new firebase.auth.FacebookAuthProvider())
            .then(res => {
                // console.log(res);
                let params: any = {
                    fbId: res.user.providerData[0].uid,
                    displayName: res.user.displayName,
                    email: res.user.email,
                    photoURL: res.user.photoURL,
                    createdDate: this.getTimestamp()
                };

                this.userInfo = res.user;
                this.updateData('profile', params, res.user.uid, false);
                return true;
            }).catch(
                err => {
                    console.error("Error: ", err);
                    alert(err.name + ", " + err.message);

                    return false;
                }
            );
    }

    logout() {
        this.afAuth.auth.signOut();
    }

    isLoggedIn(): boolean {
        return typeof this.userInfo !== 'undefined';
    }
}
