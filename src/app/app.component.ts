import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { EnvConfig } from '../environments/environment';
import { TabsPage } from '../pages/tabs/tabs';

import { HomePage } from '../pages/home/home';
import { Setting } from '../pages/setting/setting';
import * as firebase from 'firebase/app';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  pages: Array<{ title: string, component: any }>;

  constructor(private platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen) {
    
    firebase.initializeApp(EnvConfig.firebase);
    this.initializeApp();

    this.pages = [
      { title: 'Dashboard', component: TabsPage },
      { title: 'Settings', component: Setting }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {      

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      // this.statusBar.hide ();
    });
  }
}

