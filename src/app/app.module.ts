import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireModule } from 'angularfire2';
import { EnvConfig } from '../environments/environment';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { TabsPage } from '../pages/tabs/tabs';
import { FirebaseService } from '../services/firebase.service';
import { DatePipe } from '../pipes/date/date';

import { TodoModel } from '../services/todo.model';
import { UserModel } from '../services/user.model';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { Dashboard } from '../pages/dashboard/dashboard';
import { Setting } from '../pages/setting/setting';
import { Starred } from '../pages/starred/starred';
import { Profile } from '../pages/profile/profile';
import { CreateTodo } from '../pages/todo/create';
import { Detail } from '../pages/todo/detail';
import { List } from '../pages/list/list';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    Dashboard,
    Setting,
    CreateTodo,
    Starred,
    Profile,
    Detail,
    List,
    DatePipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(EnvConfig.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    Dashboard,
    Setting,
    CreateTodo,
    Starred,
    Profile,
    Detail,
    List
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseService,
    TodoModel,
    UserModel,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SplashScreen,
    StatusBar
  ]
})
export class AppModule { }
