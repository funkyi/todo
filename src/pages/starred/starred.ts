import { Component } from '@angular/core';
import { NavController, ModalController, ViewController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';
import { FirebaseListObservable } from 'angularfire2/database';
import { CreateTodo } from '../todo/create';
import { Detail } from '../todo/detail';
import { TodoModel } from '../../services/todo.model';

@Component({
    selector: 'tabs-starred',
    templateUrl: 'starred.html'
})
export class Starred {
    todolist: FirebaseListObservable<any[]>;
    lists: any = [];

    constructor(public navCtrl: NavController, private firebaseService: FirebaseService,
        private params: NavParams, private modalCtrl: ModalController, private viewCtrl: ViewController, ) {
        this.loadTodo();
    }

    loadTodo() {
        let results: any = [];

        let queryString: any = {
            query: {
                orderByChild: 'starred',
                equalTo: true
            }
        };

        this.todolist = this.firebaseService.loadTodo(
            this.firebaseService.userInfo.uid + '/todo',
            queryString);

        this.todolist.subscribe(r => {
            r.forEach(function (value) {
                let t = new TodoModel();
                t.id = value.$key;
                t.note = value.note;
                t.subject = value.subject;
                t.dateAdded = value.dateAdded;
                t.timeAdded = value.timeAdded;

                results.push (t);
            });
        });
        
        this.lists = this.firebaseService.OrderByArray (results, "dataAdded");
    }

    showDetail(id: string) {
        let detail: TodoModel = this.firebaseService.getTodoDetail (this.lists, id);
        this.navCtrl.push(Detail, { todoId: id, todoDetail: detail });
    }

    createTodo() {
        let modal = this.modalCtrl.create(CreateTodo, { numberOfPage: 1 });
        modal.present();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

}
