import { Component } from '@angular/core';

import { Dashboard } from '../dashboard/dashboard';
import { Setting } from '../setting/setting';
import { Starred } from '../starred/starred';
import { Profile } from '../profile/profile';

@Component({
    templateUrl: 'tabs.html'
})

export class TabsPage {
    tabDashboard: any = Dashboard;
    tabStarred: any = Starred;
    tabProfile: any = Profile;
    tabSetting: any = Setting;

    constructor () { }

}