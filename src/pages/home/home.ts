import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FirebaseService } from '../../services/firebase.service';
import { EnvConfig } from '../../environments/environment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  format_date = 'YYYY-MM-DD HH:mm:ss';

  constructor(public navCtrl: NavController, private fireService: FirebaseService) {
    
  }

  staySignedIn: boolean = false;

  login() {    
    if (!EnvConfig.production && this.fireService.isLoggedIn()) {
      this.navCtrl.setRoot(TabsPage);
    } else {
      this.fireService.login().then(res => {
        console.log (res);
        if (res) {
          this.navCtrl.setRoot(TabsPage);
        };
      });
    }
  }

  signOut() {
    this.fireService.logout();
  }

}
