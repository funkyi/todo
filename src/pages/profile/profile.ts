import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';

@Component({
    selector: 'tabs-profile',
    templateUrl: 'profile.html'
})
export class Profile {
    user: any;
    firstname: string;
    lastname: string;

    constructor(public navCtrl: NavController, private params: NavParams, private viewCtrl: ViewController, private firebaseService: FirebaseService) {
        this.user = firebaseService.userInfo;
        let fullname = this.user.displayName.split(" ");
        this.firstname = fullname[0];
        this.lastname = fullname[1];
        console.log (fullname[0] + ", " + fullname[1]);
        // this.lastname = this.user.displayName.splite(" ")[1];
     }

    dismiss () {
        this.viewCtrl.dismiss ();
    }
}
