import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'tabs-setting',
    templateUrl: 'setting.html'
})
export class Setting {
    gender: string = "f";
    
    constructor(public navCtrl: NavController) { }

}
