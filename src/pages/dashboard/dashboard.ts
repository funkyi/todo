import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';
import { FirebaseListObservable } from 'angularfire2/database';
import { CreateTodo } from '../todo/create';
import { List } from '../list/list';

@Component({
    selector: 'tabs-dashboard',
    templateUrl: 'dashboard.html'
})
export class Dashboard {
    todolist: FirebaseListObservable<any[]>;
    user: any;

    constructor(public navCtrl: NavController, private modalCtrl: ModalController, private firebaseService: FirebaseService) {
        this.loadTodo();
        this.user = this.firebaseService.userInfo;
    }

    loadTodo() {
        
    }

    createTodo() {
        let modal = this.modalCtrl.create(CreateTodo, { numberOfPage: 1 });
        modal.present();
    }

    goto(page: string) {
        switch (page) {
            case 'starred':
                this.navCtrl.parent.select(1);
                break;
            default:
                this.navCtrl.push (List, { 'page': page });
                break;
        }

    }
}
