import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';
import { FirebaseListObservable } from 'angularfire2/database';
import { TodoModel } from '../../services/todo.model';
import { Detail } from '../todo/detail';

@Component({
    selector: 'tabs-dashboard',
    templateUrl: 'list.html'
})
export class List {
    todolist: FirebaseListObservable<any[]>;
    lists: any = [];
    pageTitle: string;
    bgColor: string;

    constructor(public navCtrl: NavController, private firebaseService: FirebaseService,
        private params: NavParams, private viewCtrl: ViewController) {
        this.loadTodo();
    }

    loadTodo() {
        let results: any = [];
        this.pageTitle = this.params.get('page').toUpperCase();
        this.bgColor = this.params.get('page');        

        console.log("param: " + this.params.get('page'));
        let queryString: any = {
            query: {
                orderByChild: 'starred',
                equalTo: false
            }
        };

        this.todolist = this.firebaseService.loadTodo(
            this.firebaseService.userInfo.uid + '/todo',
            queryString);

        this.todolist.subscribe(r => {
            results = [];
            r.forEach(function (value) {
                let t = new TodoModel();
                t.id = value.$key;
                t.note = value.note;
                t.subject = value.subject;
                t.starred = value.starred;
                t.remark = value.remark;
                t.dateAdded = value.dateAdded;
                t.timeAdded = value.timeAdded;

                results.push (t);
                
            });

            this.lists = this.firebaseService.OrderByArray (results, "dateAdded");
        });
    }

    showDetail(id: string) {
        let detail: TodoModel = this.firebaseService.getTodoDetail (this.lists, id);
        this.navCtrl.push(Detail, { todoId: id, todoDetail: detail });
    }
}
