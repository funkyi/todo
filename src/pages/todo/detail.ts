import { Component } from '@angular/core';
import { NavController, ModalController, ViewController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';
import { TodoModel } from '../../services/todo.model';

@Component({
    selector: 'tabs-starred',
    templateUrl: 'detail.html'
})
export class Detail {    
    detail: TodoModel;  

    constructor(public navCtrl: NavController, private firebaseService: FirebaseService,
        private params: NavParams, private modalCtrl: ModalController, private viewCtrl: ViewController, ) {
        this.loadTodo();
    }

    loadTodo() {
        // let id: string = this.params.get('todoId');
        this.detail = this.params.get('todoDetail');
    }

    dt (dt: string) {
        return this.firebaseService.convertDt (dt);
    }

    removeTodo (id: string) {
        this.firebaseService.removeData (id);
        this.viewCtrl.dismiss ();
    }

}
