import { Component } from '@angular/core';
import { NavController, ModalController, ViewController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseService } from '../../services/firebase.service';

@Component({
    selector: 'tabs-dashboard',
    templateUrl: 'create.html'
})
export class CreateTodo {
    public event = {
        month: '2017-05-01',
        timeStarts: '07:43',
        timeEnds: '2200-12-31'
    }

    form;

    constructor(public navCtrl: NavController, private modal: ModalController,
        private toastCtrl: ToastController,
        private params: NavParams, private viewCtrl: ViewController,
        private firebaseService: FirebaseService ) {
        this.form = new FormGroup({
            subject: new FormControl("", Validators.required),
            dateadded: new FormControl (""),
            timeadded: new FormControl (""),
            note: new FormControl(""),
            remark: new FormControl(""),
            starred: new FormControl(false)
        });
    }

    createTodo() {

        if (this.form.status === 'VALID') {
            let params: any = {
                subject: this.form.value.subject,
                note: this.form.value.note,
                starred: this.form.value.starred,
                remark: this.form.value.remark,
                dateAdded: this.firebaseService.convertDt(this.form.value.dateadded).getTime() / 1000,
                timeAdded: this.form.value.timeadded
            };

            this.firebaseService.writeData(params);

            this.dismiss();
        }
        
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
