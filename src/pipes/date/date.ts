import { Pipe, PipeTransform } from '@angular/core';
// import { FirebaseService } from '../../services/firebase.service';

@Pipe({
  name: 'dt',
})
export class DatePipe implements PipeTransform {
  transform(timestamp: string, formats: string) {
    let dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    let monthNames = [
      "January", "February", "March",
      "April", "May", "June",
      "July", "August", "September",
      "October", "November", "December"
    ];

    let monthNo = [
      "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
    ]

    let format = formats;
    let display: string;

    let dt = new Date(parseInt(timestamp) * 1000);
    switch (format) {
      case 'full':
        display = dt.getDate() + "/" + monthNo[dt.getMonth()] + "/" + dt.getFullYear();
        break;
      case 'day':
        display = dt.getDate().toString();
        break;
      default:
        display = dt.getDate() + "/" + monthNo[dt.getMonth()] + "/" + dt.getFullYear();
        break;
    }

    return display;
  }
}
